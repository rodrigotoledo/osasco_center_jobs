json.extract! job, :id, :user_id, :protocol, :address_zipcode, :address, :address_number, :address_complement, :starts_at, :ends_at, :description, :created_at, :updated_at
json.url job_url(job, format: :json)
