class Job < ApplicationRecord
  belongs_to :user

  validates :user, :protocol, :address_zipcode, :address, :address_number, :starts_at, :ends_at, :description, presence: true
  validates :description, length: { maximum: 1000 }

  def full_address
    [address_zipcode, address, address_number].join(', ')
  end
end
