require "application_system_test_case"

class JobsTest < ApplicationSystemTestCase
  setup do
    @job = jobs(:one)
  end

  test "visiting the index" do
    visit jobs_url
    assert_selector "h1", text: "Jobs"
  end

  test "creating a Job" do
    visit jobs_url
    click_on "New Job"

    fill_in "Address", with: @job.address
    fill_in "Address Complement", with: @job.address_complement
    fill_in "Address Number", with: @job.address_number
    fill_in "Address Zipcode", with: @job.address_zipcode
    fill_in "Description", with: @job.description
    fill_in "Ends At", with: @job.ends_at
    fill_in "Protocol", with: @job.protocol
    fill_in "Starts At", with: @job.starts_at
    fill_in "User", with: @job.user_id
    click_on "Create Job"

    assert_text "Job was successfully created"
    click_on "Back"
  end

  test "updating a Job" do
    visit jobs_url
    click_on "Edit", match: :first

    fill_in "Address", with: @job.address
    fill_in "Address Complement", with: @job.address_complement
    fill_in "Address Number", with: @job.address_number
    fill_in "Address Zipcode", with: @job.address_zipcode
    fill_in "Description", with: @job.description
    fill_in "Ends At", with: @job.ends_at
    fill_in "Protocol", with: @job.protocol
    fill_in "Starts At", with: @job.starts_at
    fill_in "User", with: @job.user_id
    click_on "Update Job"

    assert_text "Job was successfully updated"
    click_on "Back"
  end

  test "destroying a Job" do
    visit jobs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Job was successfully destroyed"
  end
end
