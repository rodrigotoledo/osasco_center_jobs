class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.references :user, foreign_key: true
      t.string :protocol
      t.string :address_zipcode
      t.string :address
      t.string :address_number
      t.string :address_complement
      t.date :starts_at
      t.date :ends_at
      t.text :description

      t.timestamps
    end
  end
end
